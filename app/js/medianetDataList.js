'use strict';

var configMedianetDataList = function($routeProvider){

    $routeProvider.
    when("/home", {controller: "medianetDataListController",templateUrl: "views/home.html"}).
    when("/offices", {controller: "medianetDataListController",templateUrl: "views/offices.html"}).    
    when("/proyects", {controller: "medianetDataListController",templateUrl: "views/proyects.html"}).
    otherwise({redirectTo: '/home'})
    ;
}
 
var MedianetDataList = angular.module("medianetDataList", []).config(configMedianetDataList);