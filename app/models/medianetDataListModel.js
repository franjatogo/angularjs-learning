MedianetDataList.service("medianetDataModel", function(){

  this.getOffices = function(){
    return [
            {
                id : 0,
                pais : "ESPAÑA",
                direccion : "Av. LLano Castellano 13, Planta 5. 28034 Madrid",
                telefono : "+34 91 768 0440"
            },
            {
                id : 1,
                pais : "ESPAÑA",
                direccion : "C/ La Regenta 13, Planta Baja. 33006 Oviedo, España",
                telefono : "+34 98 520 8658"
            },
            {
                id : 2,
                pais : "MÉXICO",
                direccion : "Altavista Business Center. Col. Xoco, Del. Benito Juárez. 03330, México D.F.",
                telefono : "+52 55 4433 4000"
            },
            {
                id : 3,
                pais : "USA",
                direccion : "2828 Coral Way. 33145 Coral Gables Florida",
                telefono : "+1-646 703 2507"
            }
        ]
  }

  this.getProyects = function(){
    return [
            {
                id : 0,
                nombre : "MediaNet y BuyVIP: “Play it again, Sam…”",
            },
            {
                id : 1,
                nombre : "MediaNet Software desarrolla la plataforma de gamificación BBVA Game",
            },
            {
                id : 2,
                nombre : "OfficeView: el App de gestión de las oficinas EasyBank de BBVA",
            },
            {
                id : 3,
                nombre : "Amazon AWS publica el Caso de Estudio de PRISA y MediaNet Software",
            },
            {
                id : 4,
                nombre : "Plataforma B2B para Uralita",
            },
            {
                id : 5,
                nombre : "El Servicio de PMO de MNS, un caso de éxito en BBVA",
            }

        ]
  }

});