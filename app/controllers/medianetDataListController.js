MedianetDataList.controller("medianetDataListController", function($scope, medianetDataModel){

  // Obtenemos los datos de oficinas y proyectos.
  $scope.offices = medianetDataModel.getOffices();
  $scope.proyects = medianetDataModel.getProyects();

})